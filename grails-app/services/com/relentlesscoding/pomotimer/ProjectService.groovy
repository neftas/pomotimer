package com.relentlesscoding.pomotimer

import grails.gorm.transactions.Transactional
import groovy.time.TimeCategory

import java.text.SimpleDateFormat

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

@Transactional
class ProjectService {

    List<WorkSession> getPomosFinishedLastWeek() {
        use(TimeCategory) {
            WorkSession.findAllByStarttimeGreaterThanEqualsAndStatus(6.days.ago, DONE).sort { it.starttime }
        }
    }

    Map<Project, String> getProjectsAndTimeSpent(final List<WorkSession> workSessions) {
        Set<Project> projects = workSessions.project as Set
        projects.collectEntries { Project project ->
            [project, Utils.getHoursAndMinutesOf(workSessions
                    .findAll { it.project == project }
                    .sum { it.duration.seconds } as Integer)]
        }
    }

    List<WorkSession> getWorkSessionsFor(final Date requestedDate) {
        String searchDate = new SimpleDateFormat('ddMMyyyy').format(requestedDate)
        WorkSession.findAllByDateAndStatus(searchDate, DONE)
    }
}
