package com.relentlesscoding.pomotimer

import com.relentlesscoding.pomotimer.domain.WorkSessionStatus
import grails.gorm.transactions.Transactional

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.CANCELLED
import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

@Transactional
class TimerService {

    /**
     * Returns a {@link WorkSession} by id.
     * @param id The id of the {@link WorkSession}.
     * @return The {@link WorkSession} associated with the id or {@code null} if no such {@link WorkSession} exists.
     */
    WorkSession getWorkSessionById(String id) {
        isWorkSessionIdValid(id) ? WorkSession.get(id) : null
    }

    /**
     * Checks if {@code workSessionId} is not null or the empty string, and whether is consists only of digits.
     * @param workSessionId The work session id to check.
     * @return {@code true} if the {@code workSessionId} is valid, {@code false} otherwise.
     */
    static boolean isWorkSessionIdValid(String workSessionId) {
        workSessionId && (workSessionId ==~ /\d+/)
    }

    WorkSession insertWorkSession(Duration duration, Project project, WorkSessionStatus workSessionStatus) {
        if (!duration) throw new NullPointerException("'duration' is null")
        if (!project) throw new NullPointerException("'project' is null")
        if (!workSessionStatus) throw new NullPointerException("'workSessionStatus' is null")

        WorkSession workSession = new WorkSession(starttime: new Date(),
                duration: duration,
                project: project,
                status: workSessionStatus)
        if (workSession.save(flush: true, failOnError: true)) {
            log.debug "Saving new work session: '$workSession.id'."
            return workSession
        } else {
            log.error "Error while saving new work session: '${workSession.id}'."
            return workSession
        }
    }

    /**
     * Sets status of the passed {@link WorkSession} to cancelled.
     * @param workSession The work session to be changed.
     * @return {@code true} is the change is persisted, {@code false} otherwise.
     * @throws grails.validation.ValidationException When the change cannot be persisted.
     */
    boolean cancelWorkSession(WorkSession workSession) {
        if (!workSession) throw new NullPointerException("'workSession' is null")

        if (workSession.status == CANCELLED) {
            log.error "Work session with id '${workSession.id}' already has cancelled status"
            return false
        }

        workSession.status = CANCELLED
        if (workSession.save(flush: true, failOnError: true)) {
            log.debug "Saving cancelled work session: '${workSession.id}'."
            return true
        } else {
            log.error "Error while saving cancelled work session: '${workSession.id}'."
            return false
        }
    }

    /**
     * Changes the status of the passed {@link WorkSession} to {@code done}.
     * @param workSession The {@link WorkSession} that is changed.
     * @return {@code true} if the change was successfully persisted, {@code false} if not.
     * @throws grails.validation.ValidationException When the change cannot be persisted.
     */
    boolean finishWorkSession(WorkSession workSession) {
        if (!workSession) throw new NullPointerException("'workSession' is null")

        if (workSession.status == DONE) {
            log.error "Work session '${workSession.id}' is already set to 'done'."
            return false
        }

        workSession.status = DONE
        if (workSession.save(flush: true, failOnError: true)) {
            log.debug "Saving finished work session: '${workSession.id}'."
            return true
        } else {
            log.error "Error while saving finished work session: '${workSession.id}'."
            return false
        }
    }

    List<WorkSession> getActiveWorkSessionsForUser(User user = null) {
        // TODO (neftas): only work sessions of this user should be returned
        WorkSession.findAllByStatus(WorkSessionStatus.ACTIVE)
    }

    List<Map> findTodaysWorkSessions() {
        Date todayAtMidnight = new Date().clearTime()
        List<Map> todaysSessionsMap = WorkSession.findAllByStarttimeGreaterThanEquals(todayAtMidnight).collect { WorkSession session ->
            [starttime: session.starttime.format('yyyy-MM-dd HH:mm'),
             duration : Utils.getHoursAndMinutesOf(session.duration.seconds),
             project  : session.project.name,
             status   : session.status.name().toLowerCase()]
        }
        return todaysSessionsMap
    }
}
