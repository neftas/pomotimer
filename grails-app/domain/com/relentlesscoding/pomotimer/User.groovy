package com.relentlesscoding.pomotimer

class User {
    String firstName
    String lastName
    String userName
    String email
    String password

    static constraints = {
        firstName blank: false, nullable: false, maxSize: 100, matches: '([a-zA-Z][ -]?)*[a-zA-Z]'
        lastName blank: false, nullable: false, maxSize: 100, matches: '([a-zA-Z][ -]?)*[a-zA-Z]'
        userName blank: false, nullable: false, maxSize: 100, matches: '\\w+', unique: true
        email email: true, blank: false, nullable: false, maxSize: 256, unique: true
        password blank: false, nullable: false, maxSize: 500, minSize: 10, password: true
    }

    String toString() { return "${firstName} ${lastName} (${userName}: ${email})"}
}
