package com.relentlesscoding.pomotimer

class ProjectStatus {
    String name
    String description

    static constraints = {
        name blank: false, nullable: false, maxSize: 100, unique: true
        description blank: true, nullable: true, maxSize: 1000
    }

    String toString() { return name }
}
