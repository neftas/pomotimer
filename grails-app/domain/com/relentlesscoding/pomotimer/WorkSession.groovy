package com.relentlesscoding.pomotimer

import com.relentlesscoding.pomotimer.domain.WorkSessionStatus

class WorkSession {
    Date starttime
    Duration duration
    Project project
    WorkSessionStatus status
    String date

    static constraints = {
        starttime blank: false, nullable: false
        duration blank: false, nullable: false
        project blank: false, nullable: false
        status blank: false, nullable: false
    }

    static mapping = {
        date formula: "FORMATDATETIME(starttime, 'ddMMyyyy')"
    }
}
