package com.relentlesscoding.pomotimer

class Project {
    String name
    ProjectStatus status
    Date creationtime
    Integer totaltime
    User user

    static constraints = {
        name blank: false, nullable: false, maxSize: 100, unique: true
        status blank: true, nullable: false, display: true
        creationtime blank: false, nullable: false
        totaltime blank: false, nullable: false, min: 0
        user blank: false, nullable: false, display: true
    }

    static mapping = {
        totaltime formula: '(select ifnull(sum(select d.seconds from Duration d where ws.duration_id = d.id), 0) from Work_Session ws where ws.project_id = id and ws.status = \'DONE\')'
    }

    String toString() { return name }

}
