package com.relentlesscoding.pomotimer

class Duration {
    Integer seconds

    static constraints = {
        seconds blank: false, nullable: false, unique: true, min: 1
    }

    String toString() { return Utils.getHoursAndMinutesOf(seconds ?: 0) }
}
