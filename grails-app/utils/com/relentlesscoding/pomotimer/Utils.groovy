package com.relentlesscoding.pomotimer

import static java.util.concurrent.TimeUnit.HOURS
import static java.util.concurrent.TimeUnit.SECONDS

class Utils {

    /**
     * Converts a duration in {@see Integer} into a pretty formatted string
     * that looks like {@code "12 hours and 34 minutes"}.
     * @param timeInSeconds The duration in seconds.
     * @return A string that looks like {@code 12 hours and 34 minutes"}.
     */
    static String getHoursAndMinutesOf(int timeInSeconds) {
        if (timeInSeconds < 0) throw new IllegalArgumentException("'timeInSeconds' should be greater than 0, was ${timeInSeconds}")

        long hours = SECONDS.toHours(timeInSeconds)
        long hoursInSeconds = HOURS.toSeconds(hours)
        long minutes = SECONDS.toMinutes(timeInSeconds - hoursInSeconds)

        StringBuilder duration = new StringBuilder()
        if (hours == 1) {
            duration.append(minutes > 0 ? "${hours} hour and " : "${hours} hour")
        } else if (hours > 1) {
            duration.append(minutes > 0 ? "${hours} hours and " : "${hours} hours")
        }

        if (minutes > 0) {
            duration.append(minutes == 1 ? "1 minute" : "${minutes} minutes")
        }
        if (duration.length() == 0) {
            return "0 minutes"
        }

        return duration.toString()
    }

    /**
     * Verifies whether a string provided by a user contains only a minus sign and/or digits.
     * @param usNumber The user-provided string.
     * @return {@code true} if the usNumber contains only a minus sign and/or digits, else {@code false}.
     */
    static boolean isNumber(String usNumber) {
        if (!usNumber) throw new IllegalArgumentException("'usNumber' is null or the empty string")

        usNumber ==~ /^-?\d+$/
    }
}
