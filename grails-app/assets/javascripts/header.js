$(document).ready(function() {
    HEADER.init();
});

var HEADER = (function($) {
    const SHOW = "show";
    var statisticsDropdownButton = null,
        statisticsDropdown = null,
        prevDay = null,
        nextDay = null;

    function init() {
        statisticsDropdownButton = $("#statisticsDropdownButton");
        statisticsDropdown = $("#statisticsDropdown");

        statisticsDropdownButton.click(function() {
            statisticsDropdown.addClass(SHOW);
        });

        window.onclick = function(event) {
            if (!event.target.matches('.drop-button')) {
                var dropDowns = document.getElementsByClassName("dropdown-content");
                for (var i = 0, j = dropDowns.length; i < j; i++) {
                    var openDropDown = dropDowns[i];
                    if (openDropDown.classList.contains(SHOW)) {
                        openDropDown.classList.remove(SHOW);
                    }
                }
            }
        }
    }

    return { init: init }
}(jQuery));
