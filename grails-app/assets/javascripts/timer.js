$(document).ready(function() {
    TIMER.init();
});

var TIMER = (function($) {
    const DISABLED = "disabled";

    var startWorkSessionButton = null,
        finishWorkSessionButton = null,
        cancelWorkSessionButton = null,
        durationSelect = null,
        timer = null,
        projectSelect = null,
        oldWorkSession = null,
        statusBar = null,
        currentWorkSessionId = null,
        updateInterval = null,
        startTime = null,
        finishSound = null,
        title = document.title,
        tableArea = null,
        tableContainer = null;

    function init() {
        displayPageContents();
        initializeDomElements();
        checkForAndHandleActiveWorkSession();
    }

    function displayPageContents() {
        $(".noscript").hide();
        $(".main-content").show();
    }

    function initializeDomElements() {
        findDomElements();
        initializeStartButton();
        initializeFinishButton();
        initializeCancelButton();
        initializeDurationSelect();
    }

    function findDomElements() {
        startWorkSessionButton = $("#startWorkSessionButton");
        finishWorkSessionButton = $("#finishWorkSessionButton");
        cancelWorkSessionButton = $("#stopWorkSessionButton");
        durationSelect = $("#durationSelect");
        timer = $("#timer");
        projectSelect = $("#projectSelect");
        oldWorkSession = $("#oldWorkSession");
        statusBar = $("#statusbar");
        finishSound = document.getElementById("timerSound");
        tableArea = $("#tableArea");
        tableContainer = $("#workSessionsContainer");
    }

    function initializeStartButton() {
        startWorkSessionButton.click(function(event) {
            // don't submit form
            event.preventDefault();
            createActivePomoState();
            startTimer();
        });
    }

    function createActivePomoState() {
        disable(startWorkSessionButton);
        disable(durationSelect);
        disable(projectSelect);

        enable(cancelWorkSessionButton);
    }

    function disable(elem) {
        elem.prop(DISABLED, true);
    }

    function enable(elem) {
        elem.prop(DISABLED, false);
    }

    function startTimer() {
        var selectedDuration = durationSelect.val();
        var selectedProject = projectSelect.val();
        var selectedDurationAsDate = new Date(selectedDuration * 1000);

        disable(durationSelect);
        disable(projectSelect);

        var url = startWorkSessionButton.data("url");

        $.post({
            url: url,
            data: {
                duration: selectedDuration,
                project: selectedProject
            }
        })
            .done(function(response) {
                if (response.result === "failure") {
                    console.log(response);
                    showErrorMessage("Could not start work session!\nA work session may already be in progress.", -1);
                    createNewPomoState();
                } else {
                    console.log(response);
                    currentWorkSessionId = response.workSessionId;
                    startTime = response.workSessionStartTime;
                    showMessage("Work session successfully started!", 30);
                    showTimer(selectedDurationAsDate);
                }
            })
            .fail(function(response) {
                // TODO (neftas): show warning and abort current pomo
                alert(response);
            });
    }

    function showErrorMessage(msg, durationSec) {
        showMessage(msg, durationSec, true)
    }

    function createNewPomoState() {
        startWorkSessionButton.show();
        enable(startWorkSessionButton);

        finishWorkSessionButton.hide();
        disable(finishWorkSessionButton);

        disable(cancelWorkSessionButton);
        enable(durationSelect);
        enable(projectSelect);

        resetTimer();
    }

    /**
     * Displays a message.
     * @param {string} msg - The message to be displayed.
     * @param {int} durationSec - The number of seconds the message should be displayed on the screen. If -1, the
     *                             message will be displayed indefinitely, until another message replaces it. If
     *                             less than -1, the duration will be 5 seconds.
     * @param {boolean} isError - Whether the message is an error message and should be displayed accordingly.
     */
    function showMessage(msg, durationSec, isError) {
        if (durationSec < -1) {
            console.log("duration < -1");
            durationSec = 5;
        }

        changeMsgColor(isError);

        statusBar.text(msg);
        statusBar.show();
        if (durationSec !== -1) {
            setTimeout(function() {
                statusBar.hide();
            }, durationSec * 1000);
        }
    }

    function changeMsgColor(isError) {
        if (isError) {
            statusBar.css("background", "#dc322f");
        } else {
            statusBar.css("background", "#859900");
        }
    }

    function resetTimer() {
        // reset time to start time
        clearInterval(updateInterval);
        updateTimerDOMElem(new Date(durationSelect.val() * 1000));
    }

    function showTimer(durationAsDate) {
        var callback = function() {
            updateTimerDOMElem(durationAsDate);
            updateTime(durationAsDate);
        };
        updateInterval = setInterval(callback, 1000);
    }

    function updateTimerDOMElem(durationAsDate) {
        var formattedTime = formatTime(durationAsDate);
        document.title = formattedTime;
        timer.html(formattedTime);
    }

    function formatTime(date) {
        // we don't want localized time
        var hours = date.getUTCHours();
        var formattedHours = addLeadingZero(hours);

        var minutes = date.getMinutes();
        var formattedMinutes = addLeadingZero(minutes);

        var seconds = date.getSeconds();
        var formattedSeconds = addLeadingZero(seconds);

        return formattedHours + ":" + formattedMinutes + ":" + formattedSeconds;
    }

    /**
     * Adds a leading zero to a number that is smaller than 10. Returns the same number as a string.
     * @param {int} num - An integer.
     * @return {string} - The input integer prepended with a 0 if input was less than 10, else the exact same number.
     */
    function addLeadingZero(num) {
        return num < 10 ? "0" + num : num;
    }

    function updateTime(durationAsDate) {
        if (durationAsDate.getTime() < 1000) {
            clearInterval(updateInterval);
            playFinishSound();
            showMessage("Work session finished at " + getTimestamp() + ". Click 'finish' to store it.", -1);
            createFinishedState();
        } else {
            durationAsDate.setTime(durationSelect.val() * 1000 - (new Date().getTime() - startTime));
        }
    }

    function playFinishSound() {
        finishSound.currentTime = 0;
        finishSound.play();
    }

    function getTimestamp() {
        var d = new Date();
        var datePart = d.getFullYear() + "-" + addLeadingZero(d.getMonth() + 1) + "-" + addLeadingZero(d.getDate());
        var timePart = addLeadingZero(d.getHours()) + ":" + addLeadingZero(d.getMinutes()) + ":" + addLeadingZero(d.getSeconds());
        return datePart + " " + timePart;
    }

    function createFinishedState() {
        updateTimerDOMElem(new Date(0));

        startWorkSessionButton.hide();
        finishWorkSessionButton.show();
        enable(finishWorkSessionButton);
    }

    function initializeFinishButton() {
        disable(finishWorkSessionButton);
        finishWorkSessionButton.click(function(event) {
            event.preventDefault();
            stopFinishSound();
            storeFinishedWorkSession();
            resetTitle();
            refreshWorkSessionTable();
        });
    }

    function stopFinishSound() {
        finishSound.pause();
    }

    function storeFinishedWorkSession() {
        var url = finishWorkSessionButton.data("url");

        $.post({
            url: url,
            data: {
                workSessionId: currentWorkSessionId
            }
        })
            .done(function(response, status, jqXHR) {
                console.log(response);
                showMessage("Work session successfully saved at " + getTimestamp() + "!", -1);
                createNewPomoState();
            })
            .fail(function(response, status, jqXHR) {
                console.log(response);
                showErrorMessage("Something went horribly wrong.", -1);
                alert(response);
            });
    }

    function resetTitle() {
        document.title = title;
    }

    function refreshWorkSessionTable() {
        var tbody = $(".table-container table tbody");
        tbody.empty();

        var url = tableContainer.data("url");

        $.get(url)
            .done(function(response) {
                if (response.result === "failure") {
                    console.log(response);
                    showErrorMessage("An error occurred while trying to refresh work session table", 60);
                } else {
                    $(response.workSessions).each(function(idx, workSession) {
                        var tr = $(document.createElement("tr"));
                        $(document.createElement("td")).text(workSession.starttime).appendTo(tr);
                        $(document.createElement("td")).text(workSession.duration).appendTo(tr);
                        $(document.createElement("td")).text(workSession.project).appendTo(tr);
                        $(document.createElement("td")).text(workSession.status).appendTo(tr);

                        tbody.append(tr);
                    });
                }
            })
            .fail(function(response) {
                console.log(response);
                showErrorMessage("An error occurred while trying to refresh work session table.", 60)
            })
            .always(function() {
                tableArea.show();
            });
    }

    function initializeCancelButton() {
        disable(cancelWorkSessionButton);
        cancelWorkSessionButton.click(function(event) {
            event.preventDefault();
            stopFinishSound();
            createNewPomoState();
            cancelWorkSession();
            resetTitle();
            refreshWorkSessionTable();
        });
    }

    function cancelWorkSession() {
        var url = cancelWorkSessionButton.data("url");

        $.post({
            url: url,
            data: {
                workSessionId: currentWorkSessionId
            }
        })
            .done(function(response) {
                console.log(response);
                if (response.result === "failure") {
                    showErrorMessage("Work session could not be cancelled!\nIt may already have been cancelled in another tab.", -1);
                } else {
                    showMessage("Work session successfully cancelled at " + getTimestamp() + ".", -1);
                }
            })
            .fail(function(response) {
                console.log(response);
                showErrorMessage("Technical error while trying to cancel work session!", -1);
            });
    }

    function initializeDurationSelect() {
        durationSelect.change(function() {
            var selectedTime = new Date(durationSelect.val() * 1000);
            timer.html(formatTime(selectedTime));
        });
    }

    function checkForAndHandleActiveWorkSession() {
        // if oldWorkSession is there, we should show a running timer
        if (oldWorkSession.length) {
            var json = JSON.parse(oldWorkSession.html());
            console.log(json);
            currentWorkSessionId = json.id;
            startTime = json.starttime;
            createActivePomoState();
            reinstateOldPomo(parseInt(json.starttime) + (json.duration * 1000));
        } else {
            var selectedTime = new Date(durationSelect.val() * 1000);
            timer.html(formatTime(selectedTime));
        }
    }

    function reinstateOldPomo(endTime) {
        var currentTime = new Date().getTime();
        var diffTime = endTime - currentTime;
        if (diffTime <= 0) {
            updateTimerDOMElem(new Date(0));
            showMessage("An unfinished work session was found.", -1);
            createFinishedState();
        } else {
            var remainingTime = new Date(diffTime);
            var callback = function() {
                updateTimerDOMElem(remainingTime);
                updateTime(remainingTime);
            };
            updateInterval = setInterval(callback, 1000);
            showMessage("Restoring active work session...", -1);
        }
    }

    return {init: init}
}(jQuery));
