package com.relentlesscoding.pomotimer

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

class ProjectController {
    static scaffold = Project
    def projectService

    /**
     * Shows name of the project, current status and total time worked on it.
     */
    def statistics() {
        List projects = Project.list().sort { a, b -> a.name <=> b.name }
        List<WorkSession> allFinishedWorkSessions = WorkSession.findAllByStatus(DONE)
        String formattedTotalTime = Utils.getHoursAndMinutesOf(allFinishedWorkSessions.duration.seconds.sum() as Integer ?: 0)
        // all relevant years
        Set<Integer> years = allFinishedWorkSessions.starttime.year as Set
        // for each year, show number of work sessions finished and their total duration
        Map yearlyStats = years.collectEntries { Integer year ->
            [year + 1900, Utils.getHoursAndMinutesOf(allFinishedWorkSessions.findAll {
                it.starttime.year == year
            }.duration.seconds.sum() as Integer)]
        }
        [allProjects              : projects,
         formattedTotalTime       : formattedTotalTime,
         totalFinishedWorkSessions: allFinishedWorkSessions.size(),
         yearlyStats              : yearlyStats]
    }

    def lastweek() {
        List<WorkSession> pomosFinishedLastWeek = projectService.getPomosFinishedLastWeek()
        String formattedTimeSpentOnProjects = pomosFinishedLastWeek.isEmpty() ? '' : Utils.getHoursAndMinutesOf(pomosFinishedLastWeek.duration.seconds.sum() as Integer)
        Map<Project, String> projectsAndTimeSpent = projectService.getProjectsAndTimeSpent(pomosFinishedLastWeek)
        [pomosFinishedLastWeek       : pomosFinishedLastWeek,
         projectTime                 : projectsAndTimeSpent,
         formattedTimeSpentOnProjects: formattedTimeSpentOnProjects]
    }

    // TODO (neftas): show work sessions done on single days
    def day() {
        Date date
        String usOffset = params.offset
        int offset = 0
        if (!usOffset) {
            date = new Date()
        } else {
            if (Utils.isNumber(usOffset)) {
                offset = Integer.valueOf(usOffset)
                if (offset > 0) {
                    offset = 0
                }
            }
            Calendar c = Calendar.instance
            c.add(Calendar.DAY_OF_YEAR, offset)
            date = c.getTime()
        }

        List<WorkSession> workSessionsForASingleDay = projectService.getWorkSessionsFor(date)
        String formattedTimeSpentOnProjects = workSessionsForASingleDay.isEmpty() ? "" : Utils.getHoursAndMinutesOf(workSessionsForASingleDay.duration.seconds.sum() as Integer)
        Map<Project, String> projectsAndTimeSpent = projectService.getProjectsAndTimeSpent(workSessionsForASingleDay)
        [date                        : date,
         workSessions                : workSessionsForASingleDay,
         projectsAndTimeSpent        : projectsAndTimeSpent,
         formattedTimeSpentOnProjects: formattedTimeSpentOnProjects,
         offset                      : offset]
    }
}
