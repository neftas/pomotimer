package com.relentlesscoding.pomotimer

import grails.gorm.transactions.Transactional
import groovy.json.JsonBuilder

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.ACTIVE

@Transactional
class TimerController {
    def timerService

    private final Closure failure = this.&render.curry(status: 200,
            contentType: 'application/json',
            text: '{ "result": "failure" }')
    private final Closure success = this.&render.curry(status: 200,
            contentType: 'application/json',
            text: '{ "result": "success" }')

    def index() {
        WorkSession activeWorkSession = WorkSession.findByStatus(ACTIVE, [sort: 'starttime', order: 'desc'])
        Project lastProjectWorkedOn = WorkSession.last([sort: 'starttime', order: 'desc'])?.project
        def todayAtMidnight = new Date().clearTime()
        List<WorkSession> pomosFinishedToday = WorkSession.findAllByStarttimeGreaterThanEquals(todayAtMidnight)
        [projects           : Project.list().sort { a, b -> a.name <=> b.name },
         durations          : Duration.list().sort { a, b -> a.seconds <=> b.seconds },
         activeWorkSession  : activeWorkSession,
         lastProjectWorkedOn: lastProjectWorkedOn,
         pomosFinishedToday : pomosFinishedToday]
    }

    /**
     * Responds to AJAX request to start new work session. Requires the parameters
     * {@code duration} and {@code project} to be send.
     * @return AJAX response indicating success or failure.
     */
    def createWorkSession() {
        // TODO (neftas): user makes ten requests
        String usDuration = params.duration
        String usProject = params.project
        if (!usDuration || !usProject) {
            log.error "Missing parameter duration or project."
            failure()
            return
        }

        // check if user has already a work session going
        List<WorkSession> workSessions = timerService.getActiveWorkSessionsForUser()
        if (workSessions.size() > 0) {
            log.error "User has already active work sessions: ${workSessions}."
            failure()
            return
        }

        if (!(Utils.isNumber(usDuration))) {
            log.error "Duration is not a number: '$usDuration'."
            failure()
            return
        }

        int durationInSeconds = usDuration as int

        Duration duration = Duration.findBySeconds(durationInSeconds)
        if (!duration) {
            log.error "Passed duration is not valid: '$usDuration'.\n"
            failure()
            return
        }

        Project project = Project.get(usProject)
        if (!project) {
            log.error "Passed project is not valid: '$usProject'."
            failure()
            return
        }

        WorkSession workSession = timerService.insertWorkSession(duration, project, ACTIVE)
        if (workSession) {
            JsonBuilder success = new JsonBuilder()
            success {
                result 'success'
                workSessionId workSession.id
                workSessionStartTime workSession.starttime.getTime()
            }
            render status: 200, contentType: 'application/json', text: success.toString()
        } else {
            failure()
        }
    }

    def cancelWorkSession() {
        String usWorkSessionId = params.workSessionId

        WorkSession workSession = timerService.getWorkSessionById(usWorkSessionId)
        if (!workSession) {
            log.error "Could not find work session with id '$usWorkSessionId'."
            failure()
            return
        }

        // TODO (neftas): user should match and status should be active
        if (workSession.status != ACTIVE) {
            log.error "Work session with id '$usWorkSessionId' should be 'active' but is '$workSession.status'."
            failure()
            return
        }

        boolean isWorkSessionCancelled = timerService.cancelWorkSession(workSession)
        if (isWorkSessionCancelled) {
            success()
        } else {
            failure()
        }
    }

    def finishWorkSession() {
        String usWorkSessionId = params.workSessionId
        WorkSession workSession = timerService.getWorkSessionById(usWorkSessionId)
        if (!workSession) {
            log.error "Could not find work session with id '$usWorkSessionId'."
            failure()
            return
        }

        boolean isWorkSessionFinished = timerService.finishWorkSession(workSession)
        if (isWorkSessionFinished) {
            success()
        } else {
            failure()
        }
    }

    def getTodaysWorkSessions() {
        List<Map> todaysSessions = timerService.findTodaysWorkSessions()

        JsonBuilder json = new JsonBuilder()
        json {
            workSessions todaysSessions
        }

        render status: 200, contentType: 'application/json', text: json.toString()
    }
}
