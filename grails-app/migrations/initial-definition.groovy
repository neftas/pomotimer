databaseChangeLog = {

    changeSet(author: "svandenakker (generated)", id: "1560525781477-1") {
        createTable(tableName: "duration") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "durationPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "seconds", type: "INT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-2") {
        createTable(tableName: "project") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "projectPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "creationtime", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(100)") {
                constraints(nullable: "false")
            }

            column(name: "user_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "status_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-3") {
        createTable(tableName: "project_status") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "project_statusPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "name", type: "VARCHAR(100)") {
                constraints(nullable: "false")
            }

            column(name: "description", type: "VARCHAR(1000)")
        }
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-4") {
        createTable(tableName: "user") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "userPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "first_name", type: "VARCHAR(100)") {
                constraints(nullable: "false")
            }

            column(name: "password", type: "VARCHAR(500)") {
                constraints(nullable: "false")
            }

            column(name: "user_name", type: "VARCHAR(100)") {
                constraints(nullable: "false")
            }

            column(name: "last_name", type: "VARCHAR(100)") {
                constraints(nullable: "false")
            }

            column(name: "email", type: "VARCHAR(256)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-5") {
        createTable(tableName: "work_session") {
            column(autoIncrement: "true", name: "id", type: "BIGINT") {
                constraints(primaryKey: "true", primaryKeyName: "work_sessionPK")
            }

            column(name: "version", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "starttime", type: "timestamp") {
                constraints(nullable: "false")
            }

            column(name: "status", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }

            column(name: "project_id", type: "BIGINT") {
                constraints(nullable: "false")
            }

            column(name: "duration_id", type: "BIGINT") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-6") {
        addUniqueConstraint(columnNames: "seconds", constraintName: "UC_DURATIONSECONDS_COL", tableName: "duration")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-7") {
        addUniqueConstraint(columnNames: "name", constraintName: "UC_PROJECTNAME_COL", tableName: "project")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-8") {
        addUniqueConstraint(columnNames: "name", constraintName: "UC_PROJECT_STATUSNAME_COL", tableName: "project_status")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-9") {
        addUniqueConstraint(columnNames: "email", constraintName: "UC_USEREMAIL_COL", tableName: "user")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-10") {
        addUniqueConstraint(columnNames: "user_name", constraintName: "UC_USERUSER_NAME_COL", tableName: "user")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-11") {
        addForeignKeyConstraint(baseColumnNames: "project_id", baseTableName: "work_session", constraintName: "FK7l3do14xr2h1lalpbbkkl8370", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-12") {
        addForeignKeyConstraint(baseColumnNames: "status_id", baseTableName: "project", constraintName: "FK89ii0bpi6xcc0y8c18cq3lb1n", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "project_status")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-13") {
        addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "project", constraintName: "FKo06v2e9kuapcugnyhttqa1vpt", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "user")
    }

    changeSet(author: "svandenakker (generated)", id: "1560525781477-14") {
        addForeignKeyConstraint(baseColumnNames: "duration_id", baseTableName: "work_session", constraintName: "FKqu1e7chmtb31c9t9ijr1rw1yf", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "duration")
    }
}
