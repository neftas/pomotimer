databaseChangeLog = {
    changeSet(author: 'svandenakker', id: 'insert-durations') {
        sql('''
            INSERT INTO DURATION (id, version, seconds)
            VALUES (1, 0, 1500),
                   (2, 0, 3000), 
                   (3, 0, 4500), 
                   (4, 0, 6000)
            ''')
    }

    changeSet(author: 'svandenakker', id: 'insert-project-status') {
        sql('''
            INSERT INTO PROJECT_STATUS (id, version, name, description)
            VALUES (1, 0, 'active', 'This project should appear as a choice in the timer page'),
                   (2, 0, 'suspended', 'This project is not done yet, but should not appear in the timer page'),
                   (3, 0, 'done', 'This project is done')
            '''
        )
    }
}