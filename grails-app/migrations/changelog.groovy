databaseChangeLog = {
    include file: 'initial-definition.groovy'
    include file: 'insert-initial-data.groovy'
}