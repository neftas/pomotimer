<!doctype html>
<html lang="en" xmlns:g="http://www.w3.org/1999/XSL/Transform">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Last 7 days</title>
</head>
<body>
<div id="main" class="content">
    <h1>Work sessions finished last 7 days</h1>
    <hr/>
    <div class="summary">
        <g:if test="${pomosFinishedLastWeek.size() > 0}">
            <p>
                The last seven days you completed ${pomosFinishedLastWeek.size()} work sessions working for <strong>${formattedTimeSpentOnProjects}</strong> on <strong>${(pomosFinishedLastWeek*.project*.name as Set).size()}</strong> different projects:
            </p>
            <ul>
            <g:each in="${projectTime.entrySet()}" var="entry">
                <li>${entry.key.name} (${entry.value})</li>
            </g:each>
            </ul>
        </g:if>
        <g:elseif test="${pomosFinishedLastWeek.size() == 0}">
            <p>No work sessions completed last seven days.</p>
        </g:elseif>
    </div>
    <div class="table-container">
        <table>
            <thead>
            <tr>
                <th>Start time</th>
                <th>Duration</th>
                <th>Project</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${pomosFinishedLastWeek}" var="pomo">
                <tr>
                    <td>${pomo.starttime.format('yyyy-MM-dd HH:mm')}</td>
                    <td>${pomo.duration.seconds / 60} min</td>
                    <td>${pomo.project.name}</td>
                    <td>${pomo.status}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>