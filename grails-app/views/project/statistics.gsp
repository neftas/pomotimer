<!doctype html>
<html lang="en" xmlns:g="http://www.w3.org/1999/XSL/Transform">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Manage projects</title>
</head>
<body>
    <h1>Project overview</h1>
    <hr/>
    <div class="summary">
        <p>
            For all time, you finished a total of <strong>${totalFinishedWorkSessions}</strong> pomodori (<strong>${formattedTotalTime}</strong>).
        </p>
        <!-- TODO (neftas): show number of work sessions in each year -->
        <ul>
        <g:each in="${yearlyStats.entrySet()}" var="yearStat">
            <li>${yearStat.key}: ${yearStat.value}</li>
        </g:each>
        </ul>
    </div>
    <div class="table-container">
        <table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Total time spent</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${allProjects}" var="project">
                    <tr>
                        <td>${project.name}</td>
                        <td>${project.status.name}</td>
                        <%
                            Integer seconds = project.totaltime
                            Integer hours = seconds / 60 / 60
                            Integer minutes = (seconds / 60 as Integer) % 60
                        %>
                        <td>${hours} h ${minutes} m</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </div>
</body>
</html>