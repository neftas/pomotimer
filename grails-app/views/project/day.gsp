<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Statistics for ${date.format('yyyy-MM-dd')}</title>
</head>
<body>
<div id="main" class="content">
    <g:set var="prevOffset" value="?offset=${offset - 1}"/>
    <g:set var="nextOffset" value="?offset=${offset + 1}"/>
    <h1>
        <a href="${createLink(uri: '/project/day')}${prevOffset}" id="prevDay" class="angle angle-left" title="Show previous day">&lt;</a>
        Statistics for ${date.format('yyyy-MM-dd')}
        <a href="${createLink(uri: '/project/day')}${nextOffset}" id="nextDay" class="angle angle-right" title="Show next day">&gt;</a>
    </h1>
    <hr/>
    <div class="summary">
        <g:if test="${workSessions.size() > 0}">
            <p>
                On ${date.format('yyyy-MM-dd')}, you completed ${workSessions.size()} work sessions while working for <strong>${formattedTimeSpentOnProjects}</strong> on <strong>${(workSessions.project.name as Set).size()}</strong> different projects:
            </p>
            <ul>
            <g:each in="${projectsAndTimeSpent.entrySet()}" var="entry">
                <li>${entry.key.name} (${entry.value})</li>
            </g:each>
            </ul>
        </g:if>
        <g:elseif test="${workSessions.size() == 0}">
            <p>No work sessions completed on ${date.format('yyyy-MM-dd')}.</p>
        </g:elseif>
    </div>
    <div class="table-container">
        <table>
            <thead>
            <tr>
                <th>Start time</th>
                <th>Duration</th>
                <th>Project</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${workSessions}" var="workSession">
                <tr>
                    <td>${workSession.starttime.format('yyyy-MM-dd HH:mm')}</td>
                    <td>${workSession.duration.seconds / 60} min</td>
                    <td>${workSession.project.name}</td>
                    <td>${workSession.status}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>
</body>
</html>