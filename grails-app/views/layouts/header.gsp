<div class="navbar">
    <div class="dropdown">
        <a class="drop-button" href="${createLink(uri: '/timer/index')}">PomoTimer</a>
    </div>
    <div class="dropdown">
        <a href="#" id="statisticsDropdownButton" class="drop-button">Statistics ▼</a>
        <div id="statisticsDropdown" class="dropdown-content">
            <a href="${createLink(uri: '/project/day')}">Single day</a>
            <a href="${createLink(uri: '/project/lastweek')}">Last 7 days</a>
            <a href="${createLink(uri: '/project/statistics')}">All time</a>
        </div>
    </div>
</div>
