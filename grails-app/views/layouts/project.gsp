<!doctype html>
<html lang="en" xmlns:asset="http://www.w3.org/1999/XSL/Transform" xmlns:g="http://www.w3.org/1999/XSL/Transform">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PomoTimer: <g:layoutTitle/></title>
    <asset:stylesheet version="" src="reset.css"/>
    <asset:stylesheet version="" src="user.css"/>
</head>
<body>
<g:include view="layouts/header.gsp"/>
<g:layoutBody/>
</body>
<asset:javascript src="jquery-2.2.0.min.js"/>
<asset:javascript src="header.js"/>
</html>
