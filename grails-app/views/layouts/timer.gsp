<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PomoTimer: <g:layoutTitle/></title>
    <asset:stylesheet href="reset.css"/>
    <asset:stylesheet href="user.css"/>
</head>
<body>
    <div class="noscript">It seems your browser does not support JavaScript</div>
    <div class="main-content">
        <g:include view="layouts/header.gsp"/>
        <g:layoutBody/>
    </div><!-- .main-content -->
</body>
<asset:javascript src="jquery-2.2.0.min.js"/>
<asset:javascript src="timer.js"/>
<asset:javascript src="header.js"/>
</html>