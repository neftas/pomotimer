<!doctype html>
<html lang="en" xmlns:g="http://www.w3.org/1999/XSL/Transform">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Timer overview</title>
</head>
<body>
    <h1>Do a work session</h1>
    <hr/>
    <div id="statusbar" style="display:none;">Currently, no status message is being displayed</div>
    <form action="">
        <div>
            <label for="durationSelect">Duration</label>
            <select name="durationSelect" id="durationSelect">
                <g:each in="${durations}" var="duration">
                    <g:if test="${activeWorkSession?.duration?.seconds == duration.seconds}">
                        <option value="${duration.seconds}" selected>${duration.seconds / 60} min</option>
                    </g:if>
                    <g:else>
                        <option value="${duration.seconds}">${duration.seconds / 60} min</option>
                    </g:else>
                </g:each>
            </select>
        </div>
        <div>
            <label for="projectSelect">Project</label>
            <select name="projectSelect" id="projectSelect">
                <g:each in="${projects}" var="project">
                    <g:if test="${activeWorkSession?.project?.id == project.id}">
                        <option value="${project.id}" selected>${project.name}</option>
                    </g:if>
                    <g:else>
                        <g:if test="${project?.id == lastProjectWorkedOn?.id && project?.status?.name == 'active'}">
                            <option value="${project.id}" selected>${project.name}</option>
                        </g:if>
                        <g:elseif test="${project?.status?.name == 'active'}">
                            <option value="${project.id}">${project.name}</option>
                        </g:elseif>
                    </g:else>
                </g:each>
            </select>
        </div>
    </form>
    <div class="button-box">
        <button id="startWorkSessionButton" class="button ok" data-url="${createLink(uri: '/timer/createWorkSession')}">Start pomo</button>
        <button id="finishWorkSessionButton" class="button ok" data-url="${createLink(uri: '/timer/finishWorkSession')}" style="display: none;">Finish!</button>
        <button id="stopWorkSessionButton" class="button cancel" data-url="${createLink(uri: '/timer/cancelWorkSession')}" type="reset">Abort pomo</button>
    </div>
    <div id="timer">placeholder</div>

    <div id="tableArea" <g:if test="${pomosFinishedToday.size() == 0}">style="display: none;"</g:if>>
        <hr/>

        <div id="workSessionsContainer" class="table-container" data-url="${createLink(uri: '/timer/getTodaysWorkSessions')}">
            <table>
                <thead>
                <tr>
                    <th>Start time</th>
                    <th>Duration</th>
                    <th>Project</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${pomosFinishedToday}" var="pomo">
                    <tr>
                        <td>${pomo.starttime.format('yyyy-MM-dd HH:mm')}</td>
                        <td>${pomo.duration.seconds / 60} min</td>
                        <td>${pomo.project.name}</td>
                        <td>${pomo.status}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
    <g:if test="${activeWorkSession}">
        <div id="oldWorkSession" style="display: none">
            {
                "id": "${activeWorkSession.id}",
                "starttime": "${activeWorkSession.starttime.getTime()}",
                "duration": "${activeWorkSession.duration.seconds}",
                "project": "${activeWorkSession.project.name}",
                "status": "${activeWorkSession.status}"
            }
        </div>
    </g:if>
    <div class="audio-container" style="display: none;">
        <audio id="timerSound">
            <source src="${assetPath(src: 'school-bells.mp3')}" type="audio/mpeg">
            <source src="${assetPath(src: 'school-bells.ogg')}" type="audio/ogg">
            Cannot play audio file.
        </audio>
    </div>
</body>
</html>