# PomoTimer

A simple Grails web application that allows you to record work sessions
in the fashion of a [pomodoro timer](https://en.wikipedia.org/wiki/Pomodoro_Technique).
You can add your own durations and projects. Once a work session is started,
you cannot pause it - this is intentional (see the link above for more details
on the Pomodoro technique). When the work session is finished, a sound will be
played and you will have the option to either save the work session or discard
it.

The application provides statistics, so you can check your progress for:

* individual days
* the last 7 days
* all time

## How to build and run

#### If you want to take the application for a test drive

```bash
$ ./grailsw run-app
```

#### If you want to deploy the app on your Tomcat instance

```bash
$ ./grailsw war
```

Copy the `war` file from `./build/libs/pomotimer.war` to the `webapps`
directory of Tomcat and start Tomcat.

#### If you want to create a Docker image and start a container

```bash
$ ./grailsw war
$ docker build -t pomotimer:0.2 .
$ docker run --name pomotimer -d -p 80:80 -v "$(pwd)/prodDb.mv.db:/pomotimer/prodDb.mv.db" pomotimer:0.2
```

## Technical details

This web application was build with Grails version 3.3.

## Acknowledgements

"Old School Bell Sound" was recorded by Daniel Simion and is provided unchanged, licensed under
the [CC Attribution 3.0](https://creativecommons.org/licenses/by/3.0/).
