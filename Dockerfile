FROM openjdk:8-jre-alpine

COPY build/libs/pomotimer-0.2.war /pomotimer/

WORKDIR /pomotimer
EXPOSE 80

CMD ["java", "-Dgrails.env=prod", "-Dserver.port=80", "-jar", "pomotimer-0.2.war"]
