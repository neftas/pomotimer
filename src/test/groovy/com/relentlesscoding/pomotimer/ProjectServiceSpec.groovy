package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

class ProjectServiceSpec extends HibernateSpec {

    List<Class> getDomainClasses() { [ProjectService, WorkSession] }

    def service = new ProjectService()

    def duration = new Duration(seconds: 1500)
    def activeProjectStatus = new ProjectStatus(name: 'active', description: 'bla')
    def user = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'a' * 10)
    def project = new Project(name: 'foo', status: activeProjectStatus, creationtime: new Date(), totaltime: 0, user: user)

    def 'getProjectsAndTimeSpent should return map with projects and time spent on that project'() {
        given: '5 work sessions done at the same date with the same duration (25 min) on the same project'
        5.times {
            workSessions << new WorkSession(starttime: new Date(), duration: duration, project: aProject, status: DONE)
        }

        when:
        Map<Project, String> projectsAndTimeSpent = service.getProjectsAndTimeSpent(workSessions)

        then: 'all work sessions are related to the same project'
        projectsAndTimeSpent.size() == 1

        and: '5 * 1500 seconds (25 min) equals 2:05'
        projectsAndTimeSpent[aProject] == '2 hours and 5 minutes'

        when: 'adding a new work session for a different project on the same that that also took 25 minutes'
        workSessions << new WorkSession(starttime: new Date(), duration: duration, project: anotherProject, status: DONE)
        projectsAndTimeSpent = service.getProjectsAndTimeSpent(workSessions)

        then: 'map should contain two separate projects'
        projectsAndTimeSpent.size() == 2

        and:
        projectsAndTimeSpent[anotherProject] == '25 minutes'

        where:
        projectStatus = Mock(ProjectStatus)
        aProject = new Project(name: 'Foo', status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)
        anotherProject = new Project(name: 'Bar', status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)
        workSessions = []
    }

    def 'getWorkSessionsFor should return work sessions for specific date'() {
        given: 'a work session for a specific date stored in the database'
        new WorkSession(starttime: october20In2017.getTime(), duration: duration, project: project, status: DONE)
                .save(flush: true, failOnError: true)

        and: 'project to allow formula to calculate total time spent'
        project.refresh()

        when: 'querying the database for the 24th of October work session that is not present'
        List<WorkSession> workSessions = service.getWorkSessionsFor(october24In2017.getTime())

        then: 'the query returns an empty result'
        workSessions.isEmpty()

        when: 'querying the database for the 20th of October work session that does exist'
        workSessions = service.getWorkSessionsFor(october20In2017.getTime())

        then: 'the query returns a single item'
        workSessions.size() == 1

        and: 'the total time worked on this project is 1500 seconds (25 minutes)'
        project.totaltime == 1500

        where:
        october20In2017 = Calendar.instance.with { set(2017, OCTOBER, 20); it }
        october24In2017 = Calendar.instance.with { set(2017, OCTOBER, 24); it }
    }

    def 'should return work session sorted from oldest to newest'() {
        given: 'newest work sessions inserted first'
        3.times { idx ->
            new WorkSession(starttime: new Date(nowMs + 3 - idx), duration: duration, project: project, status: DONE)
                    .save(flush: true, failOnError: true)
        }

        when:
        List<WorkSession> workSessions = service.getPomosFinishedLastWeek()

        then: 'oldest work session comes first'
        [1L, 2L, 3L] == workSessions.collect { it.starttime.getTime() - nowMs }

        where:
        nowMs = System.currentTimeMillis()
    }
}
