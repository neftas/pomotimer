package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec

class UserUniquenessSpec extends HibernateSpec {
    List<Class> getDomainClasses() { [User] }

    def 'email address should be unique'() {
        when: 'creating a new user'
        def user = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'd' * 10)

        then: 'is a valid user'
        user.validate(['email'])

        and: 'can be saved'
        user.save(failOnError: true)

        and:
        User.count() == old(User.count()) + 1

        when: 'adding another user with the same email address'
        def anotherUser = new User(firstName: 'a', lastName: 'b', userName: 'q', email: 'a@b.com', password: 'd' * 10)

        then: 'does not validate'
        !anotherUser.validate(['email'])

        and: 'cannot be saved'
        !anotherUser.save()

        and: 'will not be counted'
        User.count() == old(User.count())
    }

    def 'userName should be unique'() {
        when: 'creating a new user with a unique userName'
        def user = new User(firstName: 'a', lastName: 'b', userName: 'unique', email: 'a@b.com', password: 'd' * 10)

        then:
        user.validate(['userName'])

        and:
        user.save()

        and:
        User.count() == old(User.count()) + 1

        when: 'adding another user with the same userName'
        def anotherUser = new User(firstName: 'a', lastName: 'b', userName: 'unique', email: 'c@d.com', password: 'd' * 10)

        then:
        !anotherUser.validate(['userName'])

        and:
        !anotherUser.save()

        and:
        User.count() == old(User.count())
    }

    def 'two users with different email addresses can be added'() {
        when:
        def one = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'd' * 10)
        def two = new User(firstName: 'a', lastName: 'b', userName: 'z', email: 'c@d.com', password: 'd' * 10)

        then:
        one.validate(['userName', 'email'])
        one.save()
        User.count() == 1

        and:
        two.validate(['userName', 'email'])
        two.save()
        User.count() == 2
    }
}
