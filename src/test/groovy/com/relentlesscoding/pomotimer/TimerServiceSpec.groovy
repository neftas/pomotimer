package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec
import grails.testing.services.ServiceUnitTest
import grails.validation.ValidationException
import spock.lang.PendingFeature
import spock.lang.Subject
import spock.lang.Unroll

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.*

@Subject(TimerService)
class TimerServiceSpec extends HibernateSpec implements ServiceUnitTest<TimerService> {

    List<Class> getDomainClasses() { [] }

    def duration
    def user
    def projectStatus
    def project

    def setup() {
        duration = new Duration(seconds: 1500)
        duration.save(flush: true, failOnError: true)
        user = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'a' * 10)
        user.save(flush: true, failOnError: true)
        projectStatus = new ProjectStatus(name: 'a', description: 'bla')
        projectStatus.save(flush: true, failOnError: true)
        project = new Project(name: 'a', status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)
        project.save(flush: true, failOnError: true)
    }

    @Unroll
    def "isWorkSessionValid with input #workSessionId should return #result"() {
        expect:
        service.isWorkSessionIdValid(workSessionId) == result

        where:
        workSessionId || result
        null          || false
        ''            || false
        'hallo'       || false
        '123abc'      || false
        'abc123'      || false
        '1a2b3c'      || false
        'a1b2c3'      || false
        '1'           || true
        '9999999999'  || true
    }

    @Unroll
    def "getWorkSessionById with input #id should return #result"() {
        expect:
        service.getWorkSessionById(id) == result

        where:
        id       || result
        null     || null
        ''       || null
        'hallo'  || null
        '123abc' || null
        'abc123' || null
        '1a2b3c' || null
        'a1b2c3' || null
        // TODO (nlstevan): find way to test happy path
    }

    def 'finishWorkSession should throw on null input'() {
        when:
        service.finishWorkSession(null)

        then:
        thrown(NullPointerException)
    }

    def 'finishWorkSession should change work session status to done if everything ok'() {
        given:
        def workSession = new WorkSession(starttime: new Date(), duration: duration, project: project, status: ACTIVE)

        when:
        def success = service.finishWorkSession(workSession)

        then:
        success
        workSession.status == DONE
    }

    def 'finishWorkSession should throw when duration is null'() {
        when:
        service.finishWorkSession(workSession)

        then:
        thrown(ValidationException)

        where:
        workSession = new WorkSession(starttime: new Date(), duration: null, project: project, status: ACTIVE)
    }

    def 'finishWorkSession should return false when work session is already set to done'() {
        expect:
        workSession.status == DONE

        when:
        def result = service.finishWorkSession(workSession)

        then:
        !result
        workSession.status == DONE

        where:
        workSession = new WorkSession(starttime: new Date(), duration: duration, project: project, status: DONE)
    }

    def 'cancelWorkSession should set work session status to cancelled'() {
        given:
        def workSession = new WorkSession(starttime: new Date(), duration: duration, project: project, status: ACTIVE)

        when:
        def result = service.cancelWorkSession(workSession)

        then:
        result
        workSession.status == CANCELLED
    }

    def 'cancelWorkSession throws when input is null'() {
        when:
        service.cancelWorkSession(null)

        then:
        thrown(NullPointerException)
    }

    def 'cancelWorkSession throws when passed work session has null for duration'() {
        when:
        service.cancelWorkSession(workSession)

        then:
        thrown(ValidationException)

        where:
        workSession = new WorkSession(starttime: new Date(), duration: null, project: project, status: ACTIVE)
    }

    def 'cancelWorkSession returns false when work session status is already cancelled'() {
        expect:
        workSession.status == CANCELLED

        when:
        def result = service.cancelWorkSession(workSession)

        then:
        !result
        workSession.status == CANCELLED

        where:
        workSession = new WorkSession(starttime: new Date(), duration: duration, project: project, status: CANCELLED)
    }

    def 'getActiveWorkSessionsForUser should return active work session'() {
        given:
        def user = Mock(User)
        new WorkSession(starttime: new Date(), duration: duration, project: project, status: ACTIVE)
                .save(failOnError: true, flush: true)

        when:
        List<WorkSession> workSessions = service.getActiveWorkSessionsForUser(user)

        then:
        workSessions.size() == 1

        when:
        new WorkSession(starttime: new Date(), duration: duration, project: project, status: ACTIVE)
                .save(failOnError: true, flush: true)
        workSessions = service.getActiveWorkSessionsForUser(user)

        then:
        workSessions.size() == 2
    }

    @PendingFeature
    def 'getActiveWorkSessionsForUser should throw on null argument'() {
        when:
        service.getActiveWorkSessionsForUser(null)

        then:
        thrown(NullPointerException)
    }

    def 'insertWorkSession'() {
        when:
        WorkSession workSession = service.insertWorkSession(duration, project, CANCELLED)

        then:
        WorkSession.count() == 1
        WorkSession.first() == workSession
    }
}
