package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec
import spock.lang.Shared

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.ACTIVE

class WorkSessionDateSpec extends HibernateSpec {

    List<Class> getDomainClasses() { [WorkSession] }

    @Shared
    def duration = new Duration(seconds: 1500)
    @Shared
    def activeProjectStatus = new ProjectStatus(name: 'active', description: 'bla')
    @Shared
    def user = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'a' * 10)
    @Shared
    def project = new Project(name: 'foo', status: activeProjectStatus, creationtime: new Date(), totaltime: 0, user: user)

    def 'workSession should have a date property'() {
        when:
        workSession.save(flush: true, failOnError: true)
        workSession.refresh()

        then:
        workSession.date == '20102017'

        where:
        october20In2017At152030 = Calendar.instance.with {
            set(2017, OCTOBER, 20, 15, 20, 30)
            it
        }
        workSession = new WorkSession(starttime: october20In2017At152030.getTime(), duration: duration, project: project, status: ACTIVE)
    }
}
