package com.relentlesscoding.pomotimer

import spock.lang.Specification
import spock.lang.Unroll

class UtilsSpec extends Specification {

    @Unroll
    def "getHoursAndMinutesOf(#input) should return #output"() {
        expect:
        Utils.getHoursAndMinutesOf(input) == output

        where:
        input                   || output
        0                       || '0 minutes'
        1500                    || '25 minutes'
        3000                    || '50 minutes'
        4500                    || '1 hour and 15 minutes'
        3600                    || '1 hour'
        7200                    || '2 hours'
        86399                   || '23 hours and 59 minutes'
        172800                  || '48 hours'
        172859                  || '48 hours'
        172860                  || '48 hours and 1 minute'
        Integer.MAX_VALUE       || '596523 hours and 14 minutes'
    }

    def 'getHoursAndMinutesOf should throw on negative input'() {
        when: 'call method with null argument'
        Utils.getHoursAndMinutesOf(-1)

        then: 'an IllegalArgumentException is thrown'
        def ex = thrown(IllegalArgumentException)
        ex.message == "'timeInSeconds' should be greater than 0, was -1"

        when:
        Utils.getHoursAndMinutesOf(Integer.MIN_VALUE)

        then:
        ex = thrown(IllegalArgumentException)
        ex.message == "'timeInSeconds' should be greater than 0, was ${Integer.MIN_VALUE}"
    }

    @Unroll
    def 'isNumber(#input) should return #output'() {
        expect:
        Utils.isNumber(input) == output

        where:
        input               || output
        '-1'                || true
        '1'                 || true
        '1234567890'        || true
        '1a'                || false
        'a1'                || false
        'a'                 || false
        'я'                 || false
    }

    def 'isNumber should throw on null'() {
        when: 'input is null'
        Utils.isNumber(null)

        then: 'IllegalArgumentException is thrown'
        thrown(IllegalArgumentException)
    }

    def 'isNumber should throw on empty string'() {
        when: 'input is empty string'
        Utils.isNumber('')

        then: 'IllegalArgumentException is thrown'
        thrown(IllegalArgumentException)
    }

}
