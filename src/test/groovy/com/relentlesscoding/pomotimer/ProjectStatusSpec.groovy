package com.relentlesscoding.pomotimer

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class ProjectStatusSpec extends Specification implements DomainUnitTest<ProjectStatus> {

    def setup() {
    }

    def cleanup() {
    }

    def 'name cannot be null'() {
        when:
        domain.name = null

        then:
        !domain.validate(['name'])

        and:
        domain.errors['name'].code == 'nullable'
    }

    def 'name cannot be blank'() {
        when:
        domain.name = ''

        then:
        !domain.validate(['name'])

        and:
        domain.errors['name'].code == 'blank'
    }

    def 'name cannot exceed 100 chars'() {
        when:
        domain.name = 'a' * 101

        then:
        domain.name.length() == 101
        !domain.validate(['name'])

        and:
        domain.errors['name'].code == 'maxSize.exceeded'
    }

    def 'name can be of size 100'() {
        when:
        domain.name = 'a' * 100

        then:
        domain.name.length() == 100
        domain.validate(['name'])
    }

    def 'description can be null'() {
        when:
        domain.description = null

        then:
        domain.validate(['description'])
    }

    def 'description can be blank'() {
        when:
        domain.description = ''

        then:
        domain.validate(['description'])
    }

    def 'description cannot be longer than 1000 chars'() {
        when:
        domain.description = 'a' * 1001

        then:
        domain.description.length() == 1001
        !domain.validate(['description'])

        and:
        domain.errors['description'].code == 'maxSize.exceeded'
    }

    def 'description can be of length 1000'() {
        when:
        domain.description = 'a' * 1000

        then:
        domain.description.length() == 1000
        domain.validate(['description'])
    }
}
