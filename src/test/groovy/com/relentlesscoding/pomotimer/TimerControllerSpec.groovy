package com.relentlesscoding.pomotimer

import com.relentlesscoding.pomotimer.domain.WorkSessionStatus
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class TimerControllerSpec extends Specification implements ControllerUnitTest<TimerController> {

    def 'getTodaysWorkSessions should return 200'() {
        given:
        controller.timerService = Mock(TimerService) {
            1 * findTodaysWorkSessions() >> [[starttime: '', duration: '', project: '', status: '']]
        }

        when:
        controller.getTodaysWorkSessions()

        then:
        status == 200

        where:
        project = Mock(Project)
        duration = new Duration(seconds: 1500)
        workSessionStatus = WorkSessionStatus.ACTIVE
        workSession = new WorkSession(starttime: new Date(), project: project, duration: duration, status: workSessionStatus)
    }

}
