package com.relentlesscoding.pomotimer

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class WorkSessionSpec extends Specification implements DomainUnitTest<WorkSession> {

    def setup() {
    }

    def cleanup() {
    }

    def 'starttime cannot be null'() {
        when:
        domain.starttime = null

        then:
        !domain.validate(['starttime'])

        and:
        domain.errors['starttime'].code == 'nullable'
    }

    def 'duration cannot be null'() {
        when:
        domain.duration = null

        then:
        !domain.validate(['duration'])

        and:
        domain.errors['duration'].code == 'nullable'
    }

    def 'project cannot be null'() {
        when:
        domain.project = null

        then:
        !domain.validate(['project'])

        and:
        domain.errors['project'].code == 'nullable'
    }

    def 'status cannot be null'() {
        when:
        domain.status = null

        then:
        !domain.validate(['status'])

        and:
        domain.errors['status'].code == 'nullable'
    }
}
