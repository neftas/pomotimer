package com.relentlesscoding.pomotimer

import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.ACTIVE

class ProjectControllerSpec extends Specification implements ControllerUnitTest<ProjectController> {

    def 'project controller check'() {
        given:
        project.status >> 'active'
        workSession.duration >> duration
        controller.projectService = Stub(ProjectService) {
            getPomosFinishedLastWeek() >> [workSession]
        }

        when:
        def result = controller.lastweek()

        then:
        status == 200
        result.pomosFinishedLastWeek.size() == 1

        where:
        duration = new Duration(seconds: 1500)
        project = Stub(Project)
        workSessionStatus = ACTIVE
        workSession = new WorkSession(starttime: new Date(), duration: duration, project: project, status: workSessionStatus)
    }
}
