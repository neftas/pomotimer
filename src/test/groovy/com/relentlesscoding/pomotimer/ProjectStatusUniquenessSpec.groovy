package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec

class ProjectStatusUniquenessSpec extends HibernateSpec {
    List<Class> getDomainClasses() { [ProjectStatus] }

    def 'name of project status should be unique'() {
        when:
        def projectStatus = new ProjectStatus(name: 'foo')

        then: 'projectStatus is valid instance'
        projectStatus.validate(['name'])

        and: 'it can be saved successfully'
        projectStatus.save()

        and: 'we have a total of 1 project statuses'
        ProjectStatus.count() == 1

        when: 'we add another project status with the same name'
        def anotherProjectStatus = new ProjectStatus(name: 'foo')

        then: 'it will not validate'
        !anotherProjectStatus.validate(['name'])

        and: 'it cannot be saved'
        !anotherProjectStatus.save()

        and: 'so it will not be added as an instance'
        ProjectStatus.count() == old(ProjectStatus.count())
    }

    def 'instances can be added when name is unique'() {
        when:
        def oneStatus = new ProjectStatus(name: 'one')
        def twoStatus = new ProjectStatus(name: 'two')
        def threeStatus = new ProjectStatus(name: 'three')

        then:
        oneStatus.validate(['name'])
        oneStatus.save()

        and:
        twoStatus.validate(['name'])
        twoStatus.save()

        and:
        threeStatus.validate(['name'])
        threeStatus.save()

        and:
        ProjectStatus.count() == 3
    }

    def 'two instances can have the same description'() {
        when:
        def one = new ProjectStatus(name: 'one', description: 'foo')
        def two = new ProjectStatus(name: 'two', description: 'foo')
        def three = new ProjectStatus(name: 'two', description: 'foo')

        then:
        one.validate(['name', 'description'])
        one.save()

        and:
        two.validate(['name', 'description'])
        two.save()

        and:
        !three.validate(['name', 'description'])
        !three.save()
    }
}
