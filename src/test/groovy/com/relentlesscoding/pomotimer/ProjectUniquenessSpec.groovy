package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec

class ProjectUniquenessSpec extends HibernateSpec {
    List<Class> getDomainClasses() { [Project] }

    def 'name should be unique'() {
        given:
        final String projectName = 'A new project'

        when: 'creating a project with a name that has not been used before'
        def user = new User(firstName: 'foo', lastName: 'bar', userName: 'baz', email: 'q@w.com', password: 'b' * 10)
        def projectStatus = new ProjectStatus(name: 'a name', description: 'a description')
        def project = new Project(name: projectName, status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)

        then: 'project is valid instance'
        project.validate()

        and:
        project.save(failOnError: true)

        and:
        Project.count() == old(Project.count()) + 1

        when: 'creating another project with the same name'
        def anotherProject = new Project(name: projectName, status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)

        then: 'new project is not valid'
        !anotherProject.validate(['name'])

        and: 'because name should be unique'
        anotherProject.errors['name'].code == 'unique'

        and: 'trying to save it should fail too'
        !anotherProject.save()

        and: 'no project has been added'
        Project.count() == old(Project.count())
    }
}
