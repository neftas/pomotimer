package com.relentlesscoding.pomotimer

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class UserSpec extends Specification implements DomainUnitTest<User> {

    def setup() {
    }

    def cleanup() {
    }

    def 'firstName cannot be null'() {
        when:
        domain.firstName = null

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'nullable'
    }

    def 'firstName cannot be blank'() {
        when:
        domain.firstName = ''

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'blank'
    }

    def 'firstName cannot exceed 100 chars'() {
        when:
        domain.firstName = 'a' * 101

        then:
        domain.firstName.length() == 101
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'maxSize.exceeded'
    }

    def 'firstName can be 100 chars'() {
        when:
        domain.firstName = 'a' * 100

        then:
        domain.firstName.length() == 100
        domain.validate(['firstName'])
    }

    def 'firstName cannot contain digits'() {
        when:
        domain.firstName = '123'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot contain special chars'() {
        when:
        domain.firstName = '<script>'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName can contain dashes'() {
        when:
        domain.firstName = 'Anne-Marie'

        then:
        domain.validate(['firstName'])
    }

    def 'firstName can contain space'() {
        when:
        domain.firstName = 'Peter Jan'

        then:
        domain.validate(['firstName'])
    }

    def 'firstName can be a single letter'() {
        when:
        domain.firstName = 'a'

        then:
        domain.validate(['firstName'])
    }

    def 'firstName cannot end with a dash'() {
        when:
        domain.firstName = 'a-'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot end with whitespace'() {
        when:
        domain.firstName = 'a '

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot consist of a single dash'() {
        when:
        domain.firstName = '-'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot begin with a dash'() {
        when:
        domain.firstName = '-a'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot begin with whitespace'() {
        when:
        domain.firstName = ' a'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot contain two consecutive dashes'() {
        when:
        domain.firstName = 'a--a'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'firstName cannot contain a consecutive dash and space character'() {
        when:
        domain.firstName = 'a- a'

        then:
        !domain.validate(['firstName'])

        and:
        domain.errors['firstName'].code == 'matches.invalid'
    }

    def 'lastName cannot be null'() {
        when:
        domain.lastName = null

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'nullable'
    }

    def 'lastName cannot be blank'() {
        when:
        domain.lastName = ''

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'blank'
    }

    def 'lastName cannot be longer than 100 chars'() {
        when:
        domain.lastName = 'a' * 101

        then:
        domain.lastName.length() == 101
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'maxSize.exceeded'
    }

    def 'lastName can be 100 chars'() {
        when:
        domain.lastName = 'a' * 100

        then:
        domain.lastName.length() == 100
        domain.validate(['lastName'])
    }

    def 'lastName cannot contain digits'() {
        when:
        domain.lastName = '123'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName can contain spaces'() {
        when:
        domain.lastName = 'van den Hoeve'

        then:
        domain.validate(['lastName'])
    }

    def 'lastName cannot contain only dashes'() {
        when:
        domain.lastName = '---'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot contain special characters'() {
        when:
        domain.lastName = '<script>'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName can be a single letter'() {
        when:
        domain.lastName = 'a'

        then:
        domain.validate(['lastName'])
    }

    def 'lastName cannot end with a dash'() {
        when:
        domain.lastName = 'a-'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot end with whitespace'() {
        when:
        domain.lastName = 'a '

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot consist of a single dash'() {
        when:
        domain.lastName = '-'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot begin with a dash'() {
        when:
        domain.lastName = '-a'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot begin with whitespace'() {
        when:
        domain.lastName = ' a'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot contain two consecutive dashes'() {
        when:
        domain.lastName = 'Blauw--Geel'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName cannot contain a consecutive dash and space character'() {
        when:
        domain.lastName = 'Blauw -Geel'

        then:
        !domain.validate(['lastName'])

        and:
        domain.errors['lastName'].code == 'matches.invalid'
    }

    def 'lastName can be a name with spaces and dashes'() {
        when:
        domain.lastName = 'Van Geselen-Berkhout'

        then:
        domain.validate(['lastName'])
    }

    def 'userName cannot be null'() {
        when:
        domain.userName = null

        then:
        !domain.validate(['userName'])

        and:
        domain.errors['userName'].code == 'nullable'
    }

    def 'userName cannot be blank'() {
        when:
        domain.userName = ''

        then:
        !domain.validate(['userName'])

        and:
        domain.errors['userName'].code == 'blank'
    }

    def 'userName cannot exceed 100 chars'() {
        when:
        domain.userName = 'a' * 101

        then:
        domain.userName.length() == 101
        !domain.validate(['userName'])

        and:
        domain.errors['userName'].code == 'maxSize.exceeded'
    }

    def 'userName can be 100 chars'() {
        when:
        domain.userName = 'a' * 100

        then:
        domain.userName.length() == 100
        domain.validate(['userName'])
    }

    def 'userName can contain only digits'() {
        when:
        domain.userName = '123'

        then:
        domain.validate(['userName'])
    }

    def 'userName cannot contain special chars'() {
        when:
        domain.userName = '<script>'

        then:
        !domain.validate(['userName'])

        and:
        domain.errors['userName'].code == 'matches.invalid'
    }

    def 'userName can contain uppercase and lowercase ascii as well as digits and underscores'() {
        when:
        domain.userName = 'aZ9_'

        then:
        domain.validate(['userName'])
    }

    def 'email cannot be null'() {
        when:
        domain.email = null

        then:
        !domain.validate(['email'])

        and:
        domain.errors['email'].code == 'nullable'
    }

    def 'email cannot be blank'() {
        when:
        domain.email = ''

        then:
        !domain.validate(['email'])

        and:
        domain.errors['email'].code == 'blank'
    }

    def 'email has to be formatted correctly'() {
        when:
        domain.email = 'abc'

        then:
        !domain.validate(['email'])

        and:
        domain.errors['email'].code == 'email.invalid'
    }

    def 'email will only take a correctly formatted email address'() {
        when:
        domain.email = 'likewecare@example.com'

        then:
        domain.validate(['email'])
    }

    def 'email cannot be longer than 256 chars'() {
        when:
        domain.email = ('a' * 256) + '@example.com'

        then:
        !domain.validate(['email'])

        and:
        domain.errors['email'].code == 'email.invalid'
    }

    def 'password cannot be null'() {
        when:
        domain.password = null

        then:
        !domain.validate(['password'])

        and:
        domain.errors['password'].code == 'nullable'
    }

    def 'password cannot be blank'() {
        when:
        domain.password = ''

        then:
        !domain.validate(['password'])

        and:
        domain.errors['password'].code == 'blank'
    }

    def 'password cannot be less than 10 chars'() {
        when:
        domain.password = 'a' * 9

        then:
        domain.password.length() == 9
        !domain.validate(['password'])

        and:
        domain.errors['password'].code == 'minSize.notmet'
    }

    def 'password can be of size 10'() {
        when:
        domain.password = 'a' * 10

        then:
        domain.password.length() == 10
        domain.validate(['password'])
    }

    def 'password cannot exceed 500 chars'() {
        when:
        domain.password = 'a' * 501

        then:
        domain.password.length() == 501
        !domain.validate(['password'])

        and:
        domain.errors['password'].code == 'maxSize.exceeded'
    }

    def 'password can be of size 500'() {
        when:
        domain.password = 'a' * 500

        then:
        domain.password.length() == 500
        domain.validate(['password'])
    }
}
