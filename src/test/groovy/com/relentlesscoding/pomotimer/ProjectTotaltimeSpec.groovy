package com.relentlesscoding.pomotimer

import grails.test.hibernate.HibernateSpec

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.ACTIVE
import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

class ProjectTotaltimeSpec extends HibernateSpec {

    def 'adding a work session should increase total time'() {
        given: 'a new project'
        def projectStatus = new ProjectStatus(name: 'foo', description: 'bar')
        def user = new User(firstName: 'foo', lastName: 'bar', userName: 'baz', email: 'q@w.com', password: 'b' * 10)
        def project = new Project(name: 'a new project', status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)

        expect: 'total time of the new project is 0 seconds'
        project.totaltime == 0

        when: 'adding a completed work session to the new project'
        def duration = new Duration(seconds: 987)
        new WorkSession(starttime: new Date(), duration: duration, project: project, status: DONE).save(failOnError: true, flush: true)
        // a refresh is required to calculate total time
        project.refresh()

        then: 'total time has increased with the amount of seconds from the work session'
        project.totaltime == 987
    }

    def 'adding a unfinished work session should not increase project total time'() {
        given: 'a new project'
        def projectStatus = new ProjectStatus(name: 'foo', description: 'bar')
        def user = new User(firstName: 'foo', lastName: 'bar', userName: 'baz', email: 'q@w.com', password: 'b' * 10)
        def project = new Project(name: 'a new project', status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)

        expect: 'total time of the new project is 0 seconds'
        project.totaltime == 0

        when: 'adding an unfinished work session to the project'
        def duration = new Duration(seconds: 987)
        new WorkSession(starttime: new Date(), duration: duration, project: project, status: ACTIVE).save(failOnError: true, flush: true)
        // a refresh is required to calculate total time
        project.refresh()

        then: 'total time has increased with the amount of seconds from the work session'
        project.totaltime == 0

        when: 'adding a completed work session to the project'
        new WorkSession(starttime: new Date(), duration: duration, project: project, status: DONE).save(failOnError: true, flush: true)
        // a refresh is required to calculate total time
        project.refresh()

        then: 'now total time should increase'
        project.totaltime == 987
    }
}
