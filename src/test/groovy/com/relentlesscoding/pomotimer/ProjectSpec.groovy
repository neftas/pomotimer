package com.relentlesscoding.pomotimer

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class ProjectSpec extends Specification implements DomainUnitTest<Project> {

    def setup() {
    }

    def cleanup() {
    }

    def 'name cannot be null'() {
        when:
        domain.name = null

        then:
        !domain.validate(['name'])
        domain.errors['name'].code == 'nullable'
    }

    def 'name cannot be blank'() {
        when:
        domain.name = ''

        then:
        !domain.validate(['name'])
        domain.errors['name'].code == 'blank'
    }

    def 'name cannot be longer than 100 chars'() {
        when:
        domain.name = 'a' * 101

        then:
        domain.name.length() == 101
        !domain.validate(['name'])
        domain.errors['name'].code == 'maxSize.exceeded'

        when:
        domain.name = 'a' * 100

        then:
        domain.name.length() == 100
        domain.validate(['name'])
        !domain.errors['name']
    }

    def 'status cannot be null'() {
        when:
        domain.status = null

        then:
        !domain.validate(['status'])

        and:
        domain.errors['status'].code == 'nullable'
    }

    def 'creationtime cannot be null'() {
        when:
        domain.creationtime = null

        then:
        !domain.validate(['creationtime'])

        and:
        domain.errors['creationtime'].code == 'nullable'
    }

    def 'totaltime cannot be null'() {
        when:
        domain.totaltime = null

        then:
        !domain.validate(['totaltime'])

        and:
        domain.errors['totaltime'].code == 'nullable'
    }

    def 'totaltime cannot be negative'() {
        when:
        domain.totaltime = -1

        then:
        domain.totaltime == -1
        !domain.validate(['totaltime'])

        and:
        domain.errors['totaltime'].code == 'min.notmet'
    }

    def 'totaltime can be positive'() {
        when:
        domain.totaltime = 1

        then:
        domain.totaltime == 1
        domain.validate(['totaltime'])

        and:
        domain.errors['totaltime']?.code == null
    }

    def 'user cannot be null'() {
        when:
        domain.user = null

        then:
        !domain.validate(['user'])

        and:
        domain.errors['user'].code == 'nullable'
    }

    def 'user can be mock object'() {
        when:
        domain.user = Mock(User)

        then:
        domain.validate(['user'])
    }
}
