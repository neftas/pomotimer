package com.relentlesscoding.pomotimer

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import spock.lang.Stepwise

@Stepwise
class DurationSpec extends Specification implements DomainUnitTest<Duration> {

    def setup() {
    }

    def cleanup() {
    }

    def 'seconds cannot be null'() {
        when:
        domain.seconds = null

        then:
        !domain.validate(['seconds'])

        and:
        domain.errors['seconds'].code == 'nullable'
    }

    def 'seconds cannot be negative'() {
        when:
        domain.seconds = -1

        then:
        domain.seconds == -1
        !domain.validate(['seconds'])

        and:
        domain.errors['seconds'].code == 'min.notmet'
    }

    def 'seconds cannot be 0'() {
        when:
        domain.seconds = 0

        then:
        domain.seconds == 0
        !domain.validate(['seconds'])

        and:
        domain.errors['seconds'].code == 'min.notmet'
    }

    def 'seconds can be positive > 0'() {
        when:
        domain.seconds = 1

        then:
        domain.seconds == 1
        domain.validate(['seconds'])
    }

    def 'basic persistence mocking'() {
        setup:
        new Duration(seconds: 1).save()
        new Duration(seconds: 2).save()

        expect:
        Duration.count() == 2
    }

    def 'number of seconds should be unique'() {
        given:
        new Duration(seconds: 3).save(flush: true)
        new Duration(seconds: 3).save(flush: true)

        expect:
        Duration.findAllBySeconds(3).size() == 1
    }

}
