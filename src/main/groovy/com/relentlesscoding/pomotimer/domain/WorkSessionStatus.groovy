package com.relentlesscoding.pomotimer.domain

enum WorkSessionStatus {
    ACTIVE,
    DONE,
    CANCELLED

    @Override
    String toString() {
        return this.name().toLowerCase()
    }
}