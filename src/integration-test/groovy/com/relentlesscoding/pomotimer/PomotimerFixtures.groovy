package com.relentlesscoding.pomotimer

import groovy.transform.TupleConstructor

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

@TupleConstructor
class PomotimerFixtures {

    def clearCollections() {
        WorkSession.executeUpdate('delete from WorkSession')
        assert WorkSession.count() == 0
        Duration.executeUpdate('delete from Duration')
        assert Duration.count() == 0
        Project.executeUpdate('delete from Project')
        assert Project.count() == 0
        ProjectStatus.executeUpdate('delete from ProjectStatus')
        assert ProjectStatus.count() == 0
        User.executeUpdate('delete from User')
        assert User.count() == 0
    }

    def user
    def projectStatus
    def project
    def duration

    def insertWorkSessionWithDateSetTo(Date date) {
        makeSureUserExists()
        makeSureProjectStatusExists()
        makeSureProjectExists()
        makeSureDurationExists()

        WorkSession workSession = new WorkSession(starttime: date, duration: duration, project: project, status: DONE)
        workSession.save(flush: true, failOnError: true)
    }

    def makeSureDurationExists() {
        duration = Duration.findBySeconds(1500)
        if (!duration) {
            duration = new Duration(seconds: 1500).save(flush: true, failOnError: true)
        }
    }

    private makeSureUserExists() {
        user = User.first()
        if (!user) {
            user = new User(firstName: 'abc', lastName: 'def', userName: 'frankie', email: 'xyz@b.com', password: 'd' * 10)
            user.save(flush: true, failOnError: true)
        }
    }

    private makeSureProjectStatusExists() {
        projectStatus = ProjectStatus.findByName('active')
        if (!projectStatus) {
            projectStatus = new ProjectStatus(name: 'active', description: 'blah')
            projectStatus.save(flush: true, failOnError: true)
        }
    }

    private makeSureProjectExists() {
        project = Project.findByName('test')
        if (!project) {
            project = new Project(name: 'test', status: projectStatus, creationtime: new Date(), totaltime: 0, user: user)
            project.save(flush: true, failOnError: true)
        }
    }
}
