package com.relentlesscoding.pomotimer

import grails.testing.mixin.integration.Integration
import grails.testing.services.ServiceUnitTest
import grails.testing.spock.OnceBefore
import grails.transaction.Rollback
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.ACTIVE

@Integration
@Rollback
class TimerIntegrationSpec extends Specification implements ServiceUnitTest<TimerService> {
    @Shared
    User user = Mock(User)
    @Shared
    ProjectStatus projectStatus
    @Shared
    Duration duration
    @Shared
    Project project
    @Shared
    Date now = new Date()

    @OnceBefore
    def setupDB() {
        duration = Duration.findBySeconds(1500)
        if (!duration) {
            duration = new Duration(seconds: 1500)
            duration.save(failOnError: true)
        }
        projectStatus = ProjectStatus.findByName('testStatus')
        if (!projectStatus) {
            projectStatus = new ProjectStatus(name: 'testStatus', description: 'is active')
            projectStatus.save(failOnError: true)
        }
        user = User.findByFirstName('a')
        if (!user) {
            user = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'a' * 10)
            user.save(failOnError: true)
        }
        project = Project.findByName('Test')
        if (!project) {
            project = new Project(name: 'Test', status: projectStatus, creationtime: now, totaltime: 0, user: user)
            project.save(failOnError: true)
        }
        assert user && duration && projectStatus && project
    }

    def 'adding a work session'() {
        given:
        def before = WorkSession.count()

        when:
        WorkSession workSession = new WorkSession(starttime: now, duration: duration, project: project, status: ACTIVE)
        workSession.save(failOnError: true)
        int after = WorkSession.count()

        then:
        before < after
        after - before == 1
        WorkSession.findByStarttime(now)

        where:
        now = new Date()
    }

    def 'insertWorkSession inserts valid work session'() {
        given:
        int numberOfWorkSessionsBefore = WorkSession.count()

        when:
        service.insertWorkSession(duration, project, ACTIVE)

        then:
        WorkSession.count() > numberOfWorkSessionsBefore
    }

    @Unroll("when input (#aDuration, #aProject, #aWorkSessionStatus) a #expectedException should be thrown")
    def 'insertWorkSession should throw NPE when some argument is null'() {
        when: 'we try to call the method with invalid arguments'
        service.insertWorkSession(aDuration, aProject, aWorkSessionStatus)

        then: 'an exception is thrown'
        thrown(expectedException)

        where: 'one or more of the arguments is null'
        aDuration | aProject | aWorkSessionStatus || expectedException
        null      | null     | null               || NullPointerException
        duration  | null     | null               || NullPointerException
        duration  | project  | null               || NullPointerException
        duration  | null     | ACTIVE             || NullPointerException
        null      | project  | null               || NullPointerException
        null      | project  | ACTIVE             || NullPointerException
        null      | null     | ACTIVE             || NullPointerException
    }

    def 'check latest inserted work session'() {
        when:
        WorkSession result = service.insertWorkSession(duration, project, ACTIVE)
        def workSession = WorkSession.findByProject(project, [sort: 'starttime', order: 'desc'])

        then:
        result.id == workSession.id
    }
}
