package com.relentlesscoding.pomotimer

import grails.testing.mixin.integration.Integration
import grails.testing.services.ServiceUnitTest
import grails.testing.spock.OnceBefore
import grails.transaction.Rollback
import spock.lang.Shared
import spock.lang.Specification

import static com.relentlesscoding.pomotimer.domain.WorkSessionStatus.DONE

@Integration
@Rollback
class ProjectIntegrationSpec extends Specification implements ServiceUnitTest<Project> {
    @Shared
    User user = Mock()
    @Shared
    ProjectStatus projectStatus
    @Shared
    Duration duration
    @Shared
    Project project
    @Shared
    Date now = new Date()

    @OnceBefore
    def setupDB() {
        duration = Duration.findBySeconds(1500)
        if (!duration) {
            duration = new Duration(seconds: 1500)
            duration.save(failOnError: true)
        }
        projectStatus = ProjectStatus.findByName('testStatus')
        if (!projectStatus) {
            projectStatus = new ProjectStatus(name: 'testStatus', description: 'is active')
            projectStatus.save(failOnError: true)
        }
        user = User.findByFirstName('a')
        if (!user) {
            user = new User(firstName: 'a', lastName: 'b', userName: 'c', email: 'a@b.com', password: 'a' * 10)
            user.save(failOnError: true)
        }
        project = Project.findByName('Test')
        if (!project) {
            project = new Project(name: 'Test', status: projectStatus, creationtime: now, totaltime: 0, user: user)
            project.save(failOnError: true)
        }
        for (_ in 0..<5) {
            System.err.println("next")
            new WorkSession(starttime: new Date(), duration: duration, project: project, status: DONE).save(failOnError: true, flush: true)
        }
        assert user && duration && projectStatus && project
    }

    def 'project total should be the total time of done work sessions'() {
        given: 'project retrieves up-to-date data'
        project.refresh()

        expect: 'derived column totaltime should contain all work sessions for that project that are done'
        project.totaltime == WorkSession.findAllByProjectAndStatus(project, DONE).duration.seconds.sum()
    }
}
