package com.relentlesscoding.pomotimer

import com.relentlesscoding.pomotimer.domain.WorkSessionStatus
import grails.testing.mixin.integration.Integration
import grails.testing.services.ServiceUnitTest
import grails.transaction.Rollback
import groovy.time.TimeCategory
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static java.util.concurrent.TimeUnit.DAYS
import static java.util.concurrent.TimeUnit.MILLISECONDS

@Integration
@Rollback
class ProjectServiceIntegrationSpec extends Specification implements ServiceUnitTest<ProjectService> {

    @Delegate
    PomotimerFixtures fixtures = new PomotimerFixtures()

    @Shared def nowMs = System.currentTimeMillis()

    def setup() {
        clearCollections()
    }

    def 'getPomosFinishedLastWeek should return work sessions that were finished less than 7 days ago'() {
        given:
        insertWorkSessionWithDateSetTo(twoWeeksAgo)

        and:
        insertWorkSessionWithDateSetTo(now)

        when: 'A collection of work sessions.'
        List<WorkSession> workSessions = service.getPomosFinishedLastWeek()

        then: 'Each work session should have status DONE and be from last week.'
        workSessions.size() == 1
        workSessions.status.every { it == WorkSessionStatus.DONE }
        use(TimeCategory) {
            workSessions.starttime.every { it > 6.days.ago }
        }

        where:
        twoWeeksMs = MILLISECONDS.convert(14, DAYS)
        now = new Date()
        twoWeeksAgo = new Date(now.time - twoWeeksMs)
    }

    @Unroll('work session created at #timeCreated #verb show up in 7 day overview')
    def 'work sessions should show up in 7 day overview if they were finished during the last 7 days including today'() {
        given:
        insertWorkSessionWithDateSetTo(timeCreated)

        expect:
        service.getPomosFinishedLastWeek().size() == count

        where:
        timeCreated                | count
        getCurrentDateMinusDays(7) | 0
        getCurrentDateMinusDays(6) | 1

        verb = count ? 'should' : 'should not'
    }

    Date getCurrentDateMinusDays(int daysAgo) {
        return new Date(nowMs - MILLISECONDS.convert(daysAgo, DAYS))
    }

    def 'should return map with projects and time spent on that project'() {
        given:
        Project project = Mock()
        WorkSession workSession = Mock()
        Duration duration = Mock()
        List<WorkSession> workSessions = [workSession]

        and:
        project.name >> 'Test'
        workSession.project = project
        duration.seconds >> 1500
        workSession.duration >> duration

        when:
        Map<Project, String> projectsAndTimeSpent = service.getProjectsAndTimeSpent(workSessions)

        then:
        projectsAndTimeSpent.size() > 0
    }
}
